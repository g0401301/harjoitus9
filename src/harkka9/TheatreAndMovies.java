/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkka9;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


/**
 *
 * @author Topi
 */
public class TheatreAndMovies {
    private String id;
    private String name;
    private ObservableList<String> movies =  FXCollections.observableArrayList();
    private Document doc;
    
    public TheatreAndMovies(String a, String b) {
        id = a;
        name = b;
    }
    
    public void addMovie(String a){
        movies.add(a);
    }
    
    public void getMovie(){
    System.out.println(movies.get(0));
    }
    
    public String getTheatreID() {
        return id;
    }
    
    public String toString() {
    return name;
    }
    
    public void listTodaysMovies(String content, ListView lw) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            doc = dBuilder.parse( new InputSource( new StringReader(content)));
            doc.getDocumentElement().normalize();
            
            
            parseCurrentData(lw);
            
            
            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(Theatre.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
   private void parseCurrentData(ListView view) {
    NodeList nodes = doc.getElementsByTagName("Show");
    
    for(int i = 0; i < nodes.getLength(); i++) { 
       Node node = nodes.item(i);
        
        if(node.getNodeType() == Node.ELEMENT_NODE) {
            Element e = (Element) node;
            String title = getValue("Title", e);
            String start = getValue("dttmShowStart", e);
            String[] parts = start.split("T");
            String time = parts[1];
            String[] parts2 = time.split(":");
            String startTime = parts2[0] + ":" + parts2[1];
            movies.add(title + " klo: " + startTime );
        }
        }
        view.setItems(movies);
    }
    
 private String getValue(String tag, Element e) {
    return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent();
    }
    
public void findMovies(String content, String date, String sTime, String eTime, ListView lv) {
    
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            doc = dBuilder.parse( new InputSource( new StringReader(content)));
            doc.getDocumentElement().normalize();
            
            
            parseCurrentData2(lv, date, sTime, eTime);
            
            
            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(Theatre.class.getName()).log(Level.SEVERE, null, ex);
        }
    

}

private void parseCurrentData2(ListView view, String d, String st, String et) {
    
    String[] splitDate = d.split("\\.");
    String[] splitStart = st.split(":");
    String[] splitEnd = et.split(":");
    
    int day = Integer.parseInt(splitDate[0]);
    int month = Integer.parseInt(splitDate[1]);
    int year = Integer.parseInt(splitDate[2]);
    int hour1 = Integer.parseInt(splitStart[0]);
    int minute1 = Integer.parseInt(splitStart[1]);
    int hour2 = Integer.parseInt(splitEnd[0]);
    int minute2 = Integer.parseInt(splitEnd[1]);
    
    GregorianCalendar c1 = new GregorianCalendar();
    GregorianCalendar c2 = new GregorianCalendar();
    c1.set(year, month, day, hour1, minute1);
    c2.set(year, month, day, hour2, minute2);
    
    
    
    
    NodeList nodes = doc.getElementsByTagName("Show");
    
    for(int i = 0; i < nodes.getLength(); i++) { 
       Node node = nodes.item(i);
        
        if(node.getNodeType() == Node.ELEMENT_NODE) {
            Element e = (Element) node;
            String title = getValue("Title", e);
            String start = getValue("dttmShowStart", e);
            String[] parts = start.split("T");
            String time = parts[1];
            String[] splitTime = time.split(":");
            String time2 = splitTime[0] + ":" + splitTime[1];
            
            int hour3 = Integer.parseInt(splitTime[0]);
            int minute3 = Integer.parseInt(splitTime[1]);
            GregorianCalendar c3 = new GregorianCalendar();
            c3.set(year, month, day, hour3, minute3);
            
            if( (c1.getTime().getTime() < c3.getTime().getTime()) && ( c3.getTime().getTime() < 
                    c2.getTime().getTime()) ) {
            movies.add(title + " klo: " + time2);
        }
        }
        }
        view.setItems(movies);
    }

}
