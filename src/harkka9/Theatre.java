/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkka9;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.ComboBox;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


/**
 *
 * @author Topi
 */
public class Theatre {
    private Document doc;
    private HashMap<String, TheatreAndMovies> map;
    
    public Theatre(String content, ComboBox combo) {
        
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            doc = dBuilder.parse( new InputSource( new StringReader(content)));
            doc.getDocumentElement().normalize();
            
            map = new HashMap();
            parseCurrentData(combo);
            
            
            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(Theatre.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void parseCurrentData(ComboBox box) {
    NodeList nodes = doc.getElementsByTagName("TheatreArea");
    
    for(int i = 0; i < nodes.getLength(); i++) { 
       Node node = nodes.item(i);
        
        if(node.getNodeType() == Node.ELEMENT_NODE) {
            Element e = (Element) node;
            String id = getValue("ID",e);
            String name = getValue("Name", e);
            box.getItems().add(new TheatreAndMovies(id, name));
        }
        }
    }
    
    private String getValue(String tag, Element e) {
    return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent();
    }
    
    public HashMap<String, TheatreAndMovies> getMap() {
    return map;
    }
    
    
    
}
