/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkka9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Topi
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private ComboBox<TheatreAndMovies> combo;
    
    @FXML
    private Button listMoviesButton;
    @FXML
    private AnchorPane outputTextArea;
    @FXML
    private ListView<String> listView;
    @FXML
    private TextField dateField;
    @FXML
    private TextField startTimeField;
    @FXML
    private TextField endTimeField;

    public FXMLDocumentController() {
    }
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            URL address = new URL("http://www.finnkino.fi/xml/TheatreAreas");
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(address.openStream()));
                
                String content = "";
                String line;
                
                while((line = br.readLine()) != null) {
                content += line + "\n";
                }
                
                Theatre t = new Theatre(content, combo);
                
                
                
            } catch (IOException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        } catch (MalformedURLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    
    }

    @FXML
    private void listMoviesAction(ActionEvent event) {
        
        if( dateField.getText().isEmpty() && startTimeField.getText().isEmpty() && 
                endTimeField.getText().isEmpty()) {
        Date currentDate = new Date();
        String today = new SimpleDateFormat("dd.MM.yyyy").format(currentDate);
        String idSelected = combo.valueProperty().getValue().getTheatreID();
        String moviesURL = "http://www.finnkino.fi/xml/Schedule/?area=" + idSelected + "&dt=" + today;
        
        try {
            URL scheduleURL = new URL(moviesURL);
            BufferedReader br2 = new BufferedReader( new InputStreamReader(scheduleURL.openStream()));
            String content2 = "";
            String line2;
            
            while((line2 = br2.readLine()) != null) {
            content2 += line2 + "\n";
            }
            listView.getItems().clear();
            combo.valueProperty().getValue().listTodaysMovies(content2, listView);
            
            
            
        } catch (MalformedURLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
        else {
        
            String date = dateField.getText();
            String start = startTimeField.getText();
            String end = endTimeField.getText();
            
            String theatreID = combo.valueProperty().getValue().getTheatreID();
            String moviesURL = "http://www.finnkino.fi/xml/Schedule/?area=" + theatreID + "&dt=" + date;
            try {
                URL address = new URL(moviesURL);
                BufferedReader br3 = new BufferedReader(new InputStreamReader(address.openStream() ));
                
                String content3 = "";
                String line3;
                
                while((line3 = br3.readLine()) != null) {
                content3 += line3 + "\n";
                }
                listView.getItems().clear();
                combo.valueProperty().getValue().findMovies(content3, date, start, end, listView);
                
                
            } catch (MalformedURLException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
            
        }
    
    }
}